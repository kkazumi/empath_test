import requests
import configparser

config_ini = configparser.ConfigParser()
config_ini.read('config.ini',encoding='utf-8')
read_keys = config_ini['KEY']

APIKEY = read_keys.get('empathkey')

url ='https://api.webempath.net/v2/analyzeWav'
payload = {'apikey': APIKEY}

def get_emo(data):
    file = {'wav': data}
    res = requests.post(url, params=payload, files=file)
    return res.json()

if __name__ == "__main__":
    wav_data = "rec.wav"
    data = open(wav_data, 'rb')
    print(get_emo(data))

