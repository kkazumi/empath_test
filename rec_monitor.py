#!/usr/bin/env python

# ライブラリの読込
import pyaudio
import wave

from voice_emo import *
import datetime


# 音データフォーマット
chunk = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 1

# 閾値
threshold = 0.03

def save_wav(filename,data):
    print("save sound file")
    out = wave.open(filename,'w')
    out.setnchannels(CHANNELS)
    out.setsampwidth(2)
    out.setframerate(RATE)
    out.writeframes(data)
    out.close()

def sound_rec(emode):
  # 音の取込開始
  p = pyaudio.PyAudio()
  stream = p.open(format = FORMAT,
      channels = CHANNELS,
      rate = RATE,
      input = True,
      frames_per_buffer = chunk
  )

  filename = "rec.wav"
  #with open(filename,'w') as f:
  all = []
  bll = []
  i=0
  ONE_SEC = int(RATE/chunk)
  while True:
      # 音データの取得
      data = stream.read(chunk)
      all.append(data)
      i+=1
      if(i>ONE_SEC):
          all.reverse()
          for t in range(0, ONE_SEC):
              if(bool(all)):
                  bll.append(all.pop())
          if(len(bll)>ONE_SEC*5):
              print("cut")
              bll.reverse()
              for tt in range(0, len(bll) - ONE_SEC*5):
                  bll.pop()
              bll.reverse()
          data=b''.join(bll)
          dt_now = datetime.datetime.now()
          filename='./rec/rec'+dt_now.strftime('%Y-%m-%d-%H-%M-%S-%f')+'.wav'
          save_wav(filename,data)
          all.reverse()
          wav_data = open(filename,"rb")
          if(emode == True):
              #print("get emo",get_emo(data))
              print(get_emo(wav_data))
          i=0

  stream.close()
  p.terminate()
  print("end rec")
  #break

if __name__ == '__main__':
  print("input empath mode 1:True or 0:False")
  emode = bool(int(input()))
  sound_rec(emode)
