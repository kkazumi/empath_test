import soundfile # to read audio file
import numpy as np
import librosa # to extract speech features
import pickle # to save model after training
from sklearn.model_selection import train_test_split # for splitting training and testing
from sklearn.neural_network import MLPClassifier # multi-layer perceptron model

RATE = 11025

#def extract_feature(data, **kwargs):
def extract_feature(file_name, **kwargs):
    mfcc = kwargs.get("mfcc")
    chroma = kwargs.get("chroma")
    mel = kwargs.get("mel")
    contrast = kwargs.get("contrast")
    tonnetz = kwargs.get("tonnetz")
    #X=np.frombuffer(data,dtype=np.float32)
    #print("X",max(X),chroma,contrast)
    with soundfile.SoundFile(file_name) as sound_file:
    #if(max(X)>0):
            #while(True):
            X = sound_file.read(dtype="float32")
            sample_rate = sound_file.samplerate
            #sample_rate = RATE
            if chroma or contrast:
                stft = np.abs(librosa.stft(X))
                #elif contrast:
                #    stft = np.abs(librosa.stft(X))
            result = np.array([])
            if mfcc:
                mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
                result = np.hstack((result, mfccs))
            if chroma:
                chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
                result = np.hstack((result, chroma))
            if mel:
                mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
                result = np.hstack((result, mel))
            if contrast:
                contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
                result = np.hstack((result, contrast))
            if tonnetz:
                tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
                result = np.hstack((result, tonnetz))
    return result

# all emotions on RAVDESS dataset
int2emotion = {
    "01": "neutral",
    "02": "calm",
    "03": "happy",
    "04": "sad",
    "05": "angry",
    "06": "fearful",
    "07": "disgust",
    "08": "surprised"
}

# we allow only these emotions ( feel free to tune this on your need )
AVAILABLE_EMOTIONS = {
    "angry",
    "sad",
    "neutral",
    "happy"
}

filename = "mlp_classifier.model"
model = pickle.load(open(filename, 'rb'))

#def extract_xdata(file):
def extract_xdata(data):
    features = extract_feature(data, mfcc=True, chroma=True, mel=True)
    #features = extract_feature(file, mfcc=True, chroma=True, mel=True)
    if(features is None):
        return None
    else:
        x=np.array(features)
        return np.reshape(x,(1, -1))


#def get_emo(file):
def get_emo(data):
    print("get emo")
    #x_data = extract_xdata(file)
    x_data = extract_xdata(data)
    print("x_data",x_data)
    if(x_data is None):
        return None
    else:
        y_pred = model.predict(x_data)
        return y_pred
